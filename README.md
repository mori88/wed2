# Lafete webapp

Demonstration application for AngularJS and Node.js


## Image references

- Background Image: https://flic.kr/p/onxgfi CC 2.0 BY SA (https://creativecommons.org/licenses/by-sa/2.0/) by Thorsten Krienke (https://www.flickr.com/photos/krienke/)

## TODO
### User-Stories
Die Event-Applikation soll die folgenden User-Stories abdecken:

1. ~~ Als Veranstalter möchte ich eine Veranstaltung (Name, Beschreibung, Zielgruppe, Beitrag, Ort, Datum/Zeit, Kommentar) erfassen können, sodass Gäste sich über meine Veranstaltung informieren können. ~~
2. ~~ Als Veranstalter möchte ich die maximale Anzahl Gäste eines Anlasses beschränken können, damit sich nicht mehr Gäste anmelden, als meine Veranstaltung Platz bieten kann. ~~
3. ~~ Als Veranstalter möchte ich sehen können, wie viele Gäste sich für meine Veranstaltung angemeldet haben und was sie mitbringen, damit ich die Veranstaltung entsprechend vorbereiten kann. ~~
4. ~~ Als Gast möchte ich eine Übersicht über die aktuellen Veranstaltungen erhalten, um mich für eine interessante Veranstaltung anmelden zu können. ~~
5. ~~ Als Gast möchte ich mich für eine Veranstaltung anmelden können, um daran teilnehmen zu können und mitzuteilen, was ich mitbringen werde oder einen Kommentar zu hinterlassen. ~~
6. ~~ Als Gast möchte ich meine Anmeldung editieren können, um etwas anderes mitbringen zu können oder den Kommentar anzupassen. ~~
7. ~~ Als Gast möchte ich eine Anmeldung canceln können, sodas der Gastgeber sieht, wer sich wieder abgemeldet hat. ~~
8. ~~ Als Gast möchte ich sehen können, was andere Gäste mitbringen, um nicht das Gleiche mitzubringen. ~~

### Testat Kriterien
Damit Sie das Testat erfolgreich abschliessen können, muss Ihre Applikation die folgenden Kriterien erfüllen:

1. ~~ Die Applikation funktioniert in Firefox und Chrome (FF 40+, GC 45+). ~~
2. ~~ Alle User-Stories wurden umgesetzt. ~~
3. ~~ Die Applikation ist als Client-Server Applikation aufgebaut. Die Client-Applikation basiert auf AngularJS/JavaScript. ~~
4. ~~ Die Applikation nutzt eine REST-Schnittstelle zum Datenaustausch mit dem Server (Persistenz). Ein Node.js-Server wird von uns vorgegeben, es ist jedoch auch erlaubt, eine eigene Implementation zu erstellen (beliebige Technologie), welche die gleiche REST-Schnittstelle anbietet. Der von uns angebotene Node.js Server darf auch verändert/erweitert werden. ~~
5. ~~ Zur Kommunikation mit dem Server wird der AngularJS Ajax Service verwendet. ~~
6. ~~ Zur Verwaltung der Events und Gäste werden Services/Repositories eingesetzt. ~~
7. ~~ Die Applikation besteht aus mindestens vier verschiedenen Views. Mindestens zwei Views manipulieren Daten (Forms). Zur Navigation zwischen den Views wird die Routing Komponente von Angular eingesetzt. ~~
8. ~~ Die Benutzeroberfläche besitzt mindestens ein funktionales, minimales Styling. CSS-Frameworks wie Foundation oder Bootstrap dürfen verwendet werden (keine Bootstrap JS!). ~~
9. ~~ Responsive Design (360px*576px - 1920px*1080). ~~
10. ~~ Für Formulare wird, wo sinnvoll, HTML5-Formvalidierung + AngularJS Formvalidierung verwendet. ~~
11. ~~ Für mindestens folgende Aktionen/Komponenten existieren Jasmine-Tests mit gemockten Services: ~~
    1. ~~ Test der Scope Variablen auf Korrektheit/Vollständigkeit im List-Controller mit mindestens 3 Datensätzen (Events). ~~
    2. ~~ Create Event View: Test über erfolgreiches Anlegen eines neuen Events ~~
    3. ~~ Add Guest View: Test über erfolgreiches Hinzufügen eines neuen Gastes ~~
    4. ~~ Test jeder Repository-Methode/API-Calls mit gemocktem $httpBackend. ~~

### Zusätzliche Hinweise/Regeln
1. ~~ Teamarbeit: Mindestens 2 Personen, höchstens 3 Personen. Alle Personen müssen sich zu gleichen Teilen am Projekt beteiligen. ~~
2. ~~ CSS Präprozessoren wie Less oder Sass sind erlaubt. ~~
3. ~~ JavaScript Precompiler wie TypeScript oder CoffeeScript sind erlaubt. Dart ist nur als Präprozessor erlaubt (nicht mit Dartium-Browser). ~~
4. ~~ RequireJS wird als Module-Loader verwendet. ~~
5. ~~ Nebst Angular sind keine anderen Client-JavaScript-Frameworks/Libraries wie z.B. jQuery, ExtJS etc. erlaubt. ~~
6. ~~ Für Page-Layouts dürfen keine CSS Floats, display-inline-block oder Tabellen eingesetzt werden. ~~
7. ~~ Die Projektsource muss auf dem HSR Gitserver oder einem öffentlichen Gitserver wie GitHub oder GitBucket abgelegt werden und dem Übungsbetreuer muss Zugriff auf das Repository freigeschalten werden. ~~