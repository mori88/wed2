define([], function () {
    'use strict';

    var GuestFactory = {
        createGuest: function(identifier) {
            return {
                name: 'Philip Schoenenberg',
                contribution: 'drinks, cake, salad or snacks',
                comment:  'we passed the exam',
                id: identifier,
                canceled: false
            };
        },
        getCleanGuest: function(guest) {
            return {
                name: guest.name,
                contribution: guest.contribution,
                comment:  guest.comment,
                id: guest.id,
                canceled: guest.canceled
            }
        },
        getCleanError: function(guest) {
            return {
                message: guest.message
            }
        }
    };

    return GuestFactory;
});