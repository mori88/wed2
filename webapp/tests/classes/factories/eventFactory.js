define([], function () {
    'use strict';

    var EventFactory = {
        createEvent: function(identifier) {
            return {
                name: 'Simons birthday',
                description: 'The greatest birthday party simon ever had',
                targetGroup: 'Friends of Simon',
                contributionsDescription: 'drinks, cake, salad or snacks',
                location: {
                    name: 'Simons house',
                    street: 'Main street 5',
                    zipCode: 8000,
                    city: 'Zurich'
                },
                times:  {
                    begin: new Date('2015-10-10T18:00:00.000Z'),
                    end: new Date('2015-10-11T02:00:00.000Z')
                },
                maximalAmountOfGuests:  null,
                id: identifier,
                guests: null
            };
        },
        getCleanEvent: function(event) {
            return {
                name: event.name,
                description: event.description,
                targetGroup: event.targetGroup,
                contributionsDescription: event.contributionsDescription,
                location: event.location,
                times:  event.times,
                maximalAmountOfGuests:  event.maximalAmountOfGuests,
                id: event.id,
                guests: event.guests
            }
        },
        getCleanError: function(event) {
            return {
                message: event.message
            }
        }
    };

    return EventFactory;
});