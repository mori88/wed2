define(['tests/factories/eventFactory','app/repository/eventRepository','libraries/angularMocks', 'libraries/angularResource'],
	function(EventFactory, EventRepository, AngularMocks) {
	'use strict';

	describe('EventRepository', function() {
        var event, $resource, $httpBackend, $timeout, EventRepoObj;

        // setup
        beforeEach(function() {
            var $injector = angular.injector(['ng', 'ngResource', 'ngMock']);
            $httpBackend = $injector.get('$httpBackend');
            $resource = $injector.get('$resource');

            EventRepoObj = EventRepository($resource);

            event = new EventRepoObj(EventFactory.createEvent(3));

            $httpBackend.when('GET', '/api/events/3').respond(EventFactory.createEvent(3));
            $httpBackend.when('POST', '/api/events/3').respond(EventFactory.createEvent(3));
            $httpBackend.when('GET', '/api/events/abvhf74n6').respond({message: "Event (id null) not found."});
        });

        afterEach(function() {
            $httpBackend.verifyNoOutstandingExpectation();
            $httpBackend.verifyNoOutstandingRequest();
        });

        describe('get()', function() {
           beforeEach(function() {
                event.$save();
                $httpBackend.flush();
            });

            describe('by object id', function() {
                it('returns the object', function() {
                    var result = null;
                    EventRepository($resource).get({id: 3}).$promise.then(function(res){
                        result =  EventFactory.getCleanEvent(res);
                    });
                    $httpBackend.flush();
                    expect(result).toEqual(EventFactory.createEvent(3));
                });
            });

            describe('by inexistent object id', function() {
                it('returns null', function() {
                    var result = null;
                    EventRepoObj.get({id: 'abvhf74n6'}).$promise.then(function(res){
                        result = EventFactory.getCleanError(res);
                    });
                    $httpBackend.flush();
                    expect(result).toEqual({message: "Event (id null) not found."});
                });
            });
        });
    });
});
