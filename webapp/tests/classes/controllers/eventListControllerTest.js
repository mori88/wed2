define(['app/controllers/event/listController', 'frameworks/angular', 'libraries/angularMocks'],
	function (EventListController, Angular, AngularMocks) {
	'use strict';

	var scope, Event;

	beforeEach(AngularMocks.inject(function($injector) {
		scope = $injector.get('$rootScope').$new();

        Event =  {
			query: function(){ return [{id: 1, name: 'Dinner'},{id: 2, name: 'Lunch'},{id: 3, name: 'Brunch'}]}
		};
	}));

	describe('EventListController', function() {
		describe('property scope', function() {
			it('contains 3 events', function() {
				var eventListController = new EventListController(scope, Event);
				expect(3).toBe(scope.events.length);
			});
		});
	});
});
