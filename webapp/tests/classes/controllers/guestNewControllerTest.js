define(['app/controllers/guest/newController', 'frameworks/angular', 'libraries/angularMocks','tests/factories/guestFactory', 'app/repository/guestRepository'],
    function (NewGuestController, Angular, AngularMocks, GuestFactory, GuestRepository) {
        'use strict';

        var scope, location, $httpBackend, $resource;

        beforeEach((function() {
            var $injector = angular.injector(['ng', 'ngResource', 'ngMock']);
            $httpBackend = $injector.get('$httpBackend');
            $resource = $injector.get('$resource');

            scope = $injector.get('$rootScope').$new();
            location = $injector.get('$location');

            $httpBackend.when('GET', '/api/events/1/guests/1').respond(GuestFactory.createGuest(1));
            $httpBackend.when('POST', '/api/events/1/guests/1').respond(GuestFactory.createGuest(1));
        }));

        describe('GuestNewController', function() {
            describe('empty guest', function() {
                it('guest should be initialized and empty', function() {
                    var newGuestController  = new NewGuestController(
                        scope,
                        location,
                        {},
                        GuestRepository($resource)
                    );
                    expect(scope.guest.length).toEqual({}.length);
                });
            });
            describe('guest id 1', function() {
                it('guest should be initialized and filled with info', function() {
                    var routeParam = {
                        id: 1,
                        eventId: 1
                    };
                    var newGuestController  = new NewGuestController(
                        scope,
                        location,
                        routeParam,
                        GuestRepository($resource)
                    );
                    $httpBackend.flush();
                    expect(GuestFactory.getCleanGuest(scope.guest)).toEqual(GuestFactory.createGuest(1));
                });
            });
        });
    });
