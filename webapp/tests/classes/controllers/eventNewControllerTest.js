define(['app/controllers/event/newController', 'frameworks/angular', 'libraries/angularMocks','tests/factories/eventFactory', 'app/repository/eventRepository'],
    function (EventNewController, Angular, AngularMocks, EventFactory, EventRepository) {
        'use strict';

        var scope, location, $httpBackend, $resource;

        beforeEach((function() {
            var $injector = angular.injector(['ng', 'ngResource', 'ngMock']);
            $httpBackend = $injector.get('$httpBackend');
            $resource = $injector.get('$resource');

            scope = $injector.get('$rootScope').$new();
            location = $injector.get('$location');

            $httpBackend.when('GET', '/api/events/1').respond(EventFactory.createEvent(1));
            $httpBackend.when('POST', '/api/events/1').respond(EventFactory.createEvent(1));
        }));

        describe('EventNewController', function() {
            describe('empty event', function() {
                it('event should be initialized and empty', function() {
                    var eventNewController = new EventNewController(
                        scope,
                        location,
                        {},
                        EventRepository($resource)
                    );
                    expect(scope.event.length).toEqual({}.length);
                });
            });
            describe('event id 1', function() {
                it('event should be initialized and filled with info', function() {
                    var routeParam = {
                        eventId: 1
                    };
                    var eventNewController = new EventNewController(
                        scope,
                        location,
                        routeParam,
                        EventRepository($resource)
                    );
                    $httpBackend.flush();
                    expect(EventFactory.getCleanEvent(scope.event)).toEqual(EventFactory.createEvent(1));
                });
            });
        });
    });
