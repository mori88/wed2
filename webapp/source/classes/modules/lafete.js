define([
		'frameworks/angular',
		'app/controllers/event/listController',
		'app/controllers/event/detailController',
		'app/controllers/event/newController',
		'app/controllers/guest/newController',
		'app/repository/eventRepository',
		'app/repository/guestRepository',
		'libraries/angularRoute',
		'libraries/angularResource'
	],
	function (
		Angular,
		EventListController,
		EventDetailController,
		NewEventController,
		NewGuestController,
		EventRepository,
		GuestRepository
	) {
	'use strict';

	/* modules */
	var Lafete = Angular.module('lafete',['ngRoute', 'ngResource']);

    /* repository */
    EventRepository.$inject = ['$resource'];
    Lafete.service('EventRepository', EventRepository);
	GuestRepository.$inject = ['$resource'];
	Lafete.service('GuestRepository', GuestRepository);

	/* controllers */
	EventListController.$inject = ['$scope', 'EventRepository'];
	Lafete.controller('EventListController', EventListController);

	EventDetailController.$inject = ['$scope', '$routeParams', 'EventRepository', 'GuestRepository'];
	Lafete.controller('EventDetailController', EventDetailController);

	NewEventController.$inject = ['$scope', '$location', '$routeParams', 'EventRepository'];
	Lafete.controller('NewEventController', NewEventController);

    NewGuestController.$inject = ['$scope', '$location', '$routeParams', 'GuestRepository'];
	Lafete.controller('NewGuestController', NewGuestController);

	/* routes */
	Lafete.config(function($routeProvider) {
		$routeProvider.when('/list', {
			controller: 'EventListController',
			templateUrl: '/views/event/list.html'
		})
		.when('/events/new', {
			controller: 'NewEventController',
			templateUrl: '/views/event/edit.html'
		})
		.when('/events/:eventId', {
			controller: 'EventDetailController',
			templateUrl: '/views/event/detail.html'
		})
        .when('/events/:eventId/edit', {
                controller: 'NewEventController',
                templateUrl: '/views/event/edit.html'
            })
		.when('/events/:eventId/guest', {
			controller: 'NewGuestController',
			templateUrl: '/views/guest/edit.html'
		})
		.when('/events/:eventId/guest/:id', {
			controller: 'NewGuestController',
			templateUrl: '/views/guest/edit.html'
		})
		.otherwise({
			redirectTo: '/list'
		});
	});

    Lafete.directive('eventForm', function() {
        return {
            restrict: 'E',
            scope: {
                event: '=model',
                add: '=add',
                dateRange: '=dateRange',
                isValidDate: '=isValidDate',
                isCapacityRight: '=isCapacityRight'
            },
            templateUrl: '/views/event/form.html'
        };
    });

	return Lafete;
});
