define([], function() {
	'use strict';

	var EventDetailController = function($scope, $routeParams, EventRepository, GuestRepository) {
		$scope.event = EventRepository.get({ id: $routeParams.eventId });

		$scope.isFull = function() {
            if (typeof $scope.event.maximalAmountOfGuests === "undefined" || !$scope.event.maximalAmountOfGuests) return false;
			return !!($scope.event.maximalAmountOfGuests <= $scope.event.totalGuests());
		};

		$scope.toggleCanceled = function(guest) {
            if(!$scope.isFull() || ($scope.isFull() && !guest.canceled)) {
                guest.canceled = !guest.canceled;
                guest.eventId = $scope.event.id;
                GuestRepository.save(guest);
            }
		};
    };

	return EventDetailController;
});
