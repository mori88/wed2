define(['app/model/event'], function(Event) {

	var NewEventController = function($scope, $location, $routeParams, EventRepository) {

        if($routeParams.eventId){
            EventRepository.get({id: $routeParams.eventId}).$promise.then(function(event){
                $scope.event = event;
            }, function(error) {
                $location.path('/list/');
            });
        } else {
            $scope.event = new EventRepository(new Event());
        }

        $scope.isValidDate = function () {
            if(!$scope.event) return true
            return $scope.event.isValidDate();
        };

		$scope.add = function() {
            $scope.event.$save().then(function(data){
                $location.path('/events/'+data.id);
            });
		};


        $scope.isCapacityRight = function() {
            if(!$scope.event) return true;
            return $scope.event.isCapacityRight();
        };
	};

	return NewEventController;
});
