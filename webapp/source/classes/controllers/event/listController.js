define([], function() {
	'use strict';

	var EventListController = function($scope, EventRepository) {
        $scope.events = EventRepository.query();
	};

	return EventListController;
});
