define([], function() {
	'use strict';

	var NewGuestController = function($scope, $location, $routeParams, Guest) {
		$scope.eventId = $routeParams.eventId;
		var params = { eventId: $scope.eventId };

		if ($routeParams.id) {
			params.id = $routeParams.id;
			$scope.guest = Guest.get(params);
		} else {
			$scope.guest = new Guest(params);
		}

		$scope.add = function() {
			$scope.guest.eventId = $routeParams.eventId;
            $scope.guest.$save().then(function() {
                $location.path('/events/'+$routeParams.eventId);
            });
		};
	};

	return NewGuestController;
});
