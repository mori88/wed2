define(function() {
    'use strict';

    var Event = function(name, description, targetGroup, contributionsDescription, location, times, maximalAmountOfGuests, id, guests, message) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.targetGroup = targetGroup;
        this.contributionsDescription = contributionsDescription;
        this.location = location;
        this.times = times;
        this.maximalAmountOfGuests = maximalAmountOfGuests;
        this.guests = guests;
        this.message = message;

        this.isValidDate = function() {
            return new Date(this.times.end) > new Date(this.times.begin);
        };
        this.isCapacityRight = function() {
            if (typeof this.maximalAmountOfGuests === "undefined" || !this.maximalAmountOfGuests) return true;
            return this.maximalAmountOfGuests >= this.totalGuests();
        };
        this.totalGuests = function() {
            if(!this.guests) return 0;
            return this.guests.reduce(function(total, guest) {
                return total + !guest.canceled;
            }, 0);
        };
    };

    /**
     * Create Event object from data transfer object (json object)
     */
    Event.createFromDTO = function(jsonData) {
        return eventDateTransformer(new Event(
            jsonData.name,
            jsonData.description,
            jsonData.targetGroup,
            jsonData.contributionsDescription,
            jsonData.location,
            jsonData.times,
            jsonData.maximalAmountOfGuests,
            jsonData.id,
            jsonData.guests,
            jsonData.message
        ));
    };

    var eventDateTransformer = function(event){
        if(!event.times) return event;
        event.times.begin = new Date(event.times.begin);
        event.times.end = new Date(event.times.end);
        return event;
    };

    return Event;
});
