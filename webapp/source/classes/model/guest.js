define([], function() {
	'use strict';

	var Guest = function(name, contribution, comment, id, canceled) {
		this.id = id;
		this.name = name;
		this.contribution = contribution;
		this.comment = comment;
		this.canceled = !!canceled;
	};

	/**
	 * Create Event object from data transfer object (json object)
	 */
	Guest.createFromDTO = function(jsonData) {
		return new Guest(
			jsonData.name,
			jsonData.contribution,
			jsonData.comment,
			jsonData.id,
			jsonData.canceled
		);
	};
	return Guest;
});
