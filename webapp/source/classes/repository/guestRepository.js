define(['app/model/guest'], function(Guest) {
    'use strict';

    var GuestRepository = function($resource) {
        var guest = $resource('/api/events/:eventId/guests/:id', { eventId: '@eventId', id: '@id' }, {
            get: {
                method: 'GET',
                isArray: false,
                transformResponse: function (data, header) {
                    return Guest.createFromDTO(angular.fromJson(data));
                }
            },
            save: {
                method: 'POST',
                transformRequest: function (data, header) {
                    return angular.toJson(Guest.createFromDTO(data));
                },
                transformResponse: function (data, header) {
                    return Guest.createFromDTO(angular.fromJson(data));
                }
            },
            query: {
                method: 'GET',
                isArray: true,
                transformResponse: function (data, header) {
                    var guests = angular.fromJson(data);
                    angular.forEach(guests.items, function (item, idx) {
                        item = Guest.createFromDTO(item);
                    });
                    return guests;
                }
            }
        });
        return guest;
    };
    return GuestRepository;
});
