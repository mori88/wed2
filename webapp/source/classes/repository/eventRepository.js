define(['app/model/event'], function(Event) {
    'use strict';

    var EventRepository = function($resource) {
        var event = $resource('/api/events/:id', {id: '@id'}, {

            get: {
                method: 'GET',
                isArray: false,
                transformResponse: function (data, header) {
                    return Event.createFromDTO(angular.fromJson(data));
                }
            },
            save: {
                method: 'POST',
                transformRequest: function (data, header) {
                    return angular.toJson(Event.createFromDTO(data));
                },
                transformResponse: function (data, header) {
                    return Event.createFromDTO(angular.fromJson(data));
                }
            },
            query: {
                method: 'GET',
                isArray: true,
                transformResponse: function (data, header) {
                    var events = angular.fromJson(data);
                    angular.forEach(events, function (item, idx) {
                        events[idx] = Event.createFromDTO(item);
                    });
                    return events;
                }
            }
        });
        return event;
    }
    return EventRepository;
});
