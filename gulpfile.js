'use strict';

var gulp = require('gulp');
var server = require('gulp-express');
var open = require('gulp-open');
var sass = require('gulp-sass');

var assetsPath = './webapp/source/styles/'
gulp.task('run', ['server', 'client']);


gulp.task('server', function(cb) {
    server.run([ './server.js']);

    gulp.watch([assetsPath + '/scss/**/*.scss'], ['sass'], server.notify);
    cb(null);
});


gulp.task('client', ['sass'], function(cb){
    gulp.src(__filename)
        .pipe(open({uri: 'http://localhost:8080'}));

    cb(null);
});


gulp.task('sass', function (cb) {
    gulp.src([assetsPath + '/scss/**/*.scss'])
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest(assetsPath));
    cb(null);
});

gulp.task('sass:watch', function () {
    gulp.watch(assetsPath + '/scss/**/*.scss', ['sass']);
});

gulp.task('test', function(cb){
    gulp.src('./webapp/tests/index.html')
        .pipe(open());

    cb(null);
});